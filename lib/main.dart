import 'package:flutter/material.dart';

void main () => runApp(
    NewREG()
);

class NewREG extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      home: Scaffold(
        appBar: buildAppBarWidget(),

        body:
        buildBodyWidget(),
        // gridTest(),

      ),
    );
  }
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.white,
    title: TextButton.icon(onPressed: () {},
      icon: Icon(Icons.format_list_bulleted),
      label: Text("Menu"),
    ),
    // leading:
    //   IconButton(
    //     icon: Icon(Icons.format_list_bulleted),
    //     color: Colors.black,
    //     onPressed: () {
    //       print("Menu is calling ...");
    //     },
    //   ),
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child: Image.network(
              "https://randomc.net/image/Spy%20x%20Family/Spy%20x%20Family%20-%2004%20-%20Large%2005.jpg",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 200,
            child: Row (
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child:profileNameItems(),
                )

              ],
            ),
          ),

          Divider(
            color: Colors.grey,
          ),
          btnDetailsItems1(),

          Divider(
            color: Colors.white,
          ),
          btnDetailsItems2(),

          Divider(
            color: Colors.white,
          ),
          btnDetailsItems3(),

          Divider(
            color: Colors.white,
          ),
          btnDetailsItems4(),

          Divider(
            color: Colors.grey,
          ),

        ],
      ),
    ],
  );
}

Widget profileNameItems() {
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildNameTxt(),
      buildFacultyTxt(),
      buildMajorTxt(),
      advisorItems(),
    ],
  );
}


Widget buildNameTxt() {
  return Text(
    "ชื่อ: นางสาวอัณญา ฟอร์เจอร์",
    style: TextStyle(fontSize: 25),
  );
}

Widget buildFacultyTxt() {
  return Text(
    "คณะ: คณะวิทยาการสารสนเทศ",
    style: TextStyle(fontSize: 20),
  );
}

Widget buildMajorTxt() {
  return Text(
    "สาขาวิชา: วิทยาการคอมพิวเตอร์",
    style: TextStyle(fontSize: 20),
  );
}

Widget buildAdvisorTxt() {
  return Text("อาจารย์ที่ปรึกษา: ");
}

Widget buildAdvisorBtn() {
  return TextButton(
      onPressed: () {},
      child: TextButton.icon(
          onPressed: () {},
          icon: Icon(Icons.search),
          label: Text("ดูรายละเอียด")
      )
  );
}

Widget advisorItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildAdvisorTxt(),
      buildAdvisorBtn(),
    ],
  );
}

Widget btnDetailsItems1() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildScheduleBtn(),
      buildRegisterBtn(),
      buildMaintainStatusBtn(),
    ],
  );
}

Widget btnDetailsItems2() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildScholarshipBtn(),
      buildStudentloanBtn(),
      buildDormitoryBtn(),
    ],
  );
}

Widget btnDetailsItems3() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildInternshipBtn(),
      buildThesisBtn(),
      buildLibraryBtn(),
    ],
  );
}

Widget btnDetailsItems4() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildInboxBtn(),
      buildNewsBtn(),
      buildCalendarBtn(),
    ],
  );
}

Widget buildScheduleBtn() {
  return OutlinedButton(
    child: Text(
      "ตารางเรียน",
      style: TextStyle(
        color: Colors.lightBlueAccent.shade400,
      ),
    ),
    onPressed: () {},
  );
}

Widget buildRegisterBtn() {
  return OutlinedButton(
    child: Text(
      "ลงทะเบียน",
      style: TextStyle(
        color: Colors.lightBlueAccent.shade400,
      ),
    ),
    onPressed: () {},
  );
}

Widget buildMaintainStatusBtn() {
  return OutlinedButton(
    child: Text(
      "รักษาสภาพ",
      style: TextStyle(
        color: Colors.grey,
      ),
    ),
    onPressed: () {},
  );
}

Widget buildScholarshipBtn() {
  return OutlinedButton(
    child: Text(
      "ขอทุน",
      style: TextStyle(
        color: Colors.lightBlueAccent.shade400,
      ),
    ),
    onPressed: () {},
  );
}

Widget buildStudentloanBtn() {
  return OutlinedButton(
    child: Text(
      "กยศ.",
      style: TextStyle(
        color: Colors.lightBlueAccent.shade400,
      ),
    ),
    onPressed: () {},
  );
}

Widget buildDormitoryBtn() {
  return OutlinedButton(
    child: Text(
      "หอพัก",
      style: TextStyle(
        color: Colors.grey,
      ),
    ),
    onPressed: () {},
  );
}

Widget buildInternshipBtn() {
  return OutlinedButton(
    child: Text(
      "INTERNSHIP",
      style: TextStyle(
        color: Colors.lightBlueAccent.shade400,
      ),
    ),
    onPressed: () {},
  );
}

Widget buildThesisBtn() {
  return OutlinedButton(
    child: Text(
      "THESIS",
      style: TextStyle(
        color: Colors.grey,
      ),
    ),
    onPressed: () {},
  );
}

Widget buildLibraryBtn() {
  return OutlinedButton(
    child: Text(
      "ห้องสมุด",
      style: TextStyle(
        color: Colors.lightBlueAccent.shade400,
      ),
    ),
    onPressed: () {},
  );
}

Widget buildInboxBtn() {
  return OutlinedButton(
    child: Text(
      "กล่องขาเข้า",
      style: TextStyle(
        color: Colors.lightBlueAccent.shade400,
      ),
    ),
    onPressed: () {},
  );
}

Widget buildNewsBtn() {
  return OutlinedButton(
    child: Text(
      "ข่าวประชาสัมพันธ์",
      style: TextStyle(
        color: Colors.lightBlueAccent.shade400,
      ),
    ),
    onPressed: () {},
  );
}

Widget buildCalendarBtn() {
  return OutlinedButton(
    child: Text(
      "ปฏิทินการศึกษา",
      style: TextStyle(
        color: Colors.lightBlueAccent.shade400,
      ),
    ),
    onPressed: () {},
  );
}

// Widget gridTest() {
//   return CustomScrollView(
//     primary: false,
//     slivers: <Widget>[
//       SliverPadding(
//         padding: const EdgeInsets.all(20),
//         sliver: SliverGrid.count(
//           crossAxisSpacing: 10,
//           mainAxisSpacing: 10,
//           crossAxisCount: 2,
//           children: <Widget>[
//             Container(
//               padding: const EdgeInsets.all(8),
//               color: Colors.green[100],
//               child: const Text("He'd have you all unravel at the"),
//             ),
//             Container(
//               padding: const EdgeInsets.all(8),
//               color: Colors.green[200],
//               child: const Text('Heed not the rabble'),
//             ),
//             Container(
//               padding: const EdgeInsets.all(8),
//               color: Colors.green[300],
//               child: const Text('Sound of screams but the'),
//             ),
//             Container(
//               padding: const EdgeInsets.all(8),
//               color: Colors.green[400],
//               child: const Text('Who scream'),
//             ),
//             Container(
//               padding: const EdgeInsets.all(8),
//               color: Colors.green[500],
//               child: const Text('Revolution is coming...'),
//             ),
//             Container(
//               padding: const EdgeInsets.all(8),
//               color: Colors.green[600],
//               child: const Text('Revolution, they...'),
//             ),
//           ],
//         ),
//       ),
//     ],
//   );
// }